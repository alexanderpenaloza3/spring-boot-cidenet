package com.alexander.cidenet.backend.controller;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alexander.cidenet.backend.model.Area;
import com.alexander.cidenet.backend.service.AreaService;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/api/areas")
public class AreaController {

	// Inyección del servicio de areas
	@Autowired 
	private AreaService areaService;
	
	// Enviar areas por medio de la petición GET
	@GetMapping
	public List<Area> list() {
		List<Area> areas = StreamSupport.stream(areaService.findAll().spliterator(), false).collect(Collectors.toList());
		return areas;
	}
	
	// Crear nueva area recibida por medio de la petición POST
	@PostMapping
	public ResponseEntity<?> create(@RequestBody Area area) {
		return ResponseEntity.status(HttpStatus.CREATED).body(areaService.save(area));
	}
}
