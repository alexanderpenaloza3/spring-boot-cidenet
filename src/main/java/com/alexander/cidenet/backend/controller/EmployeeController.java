package com.alexander.cidenet.backend.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alexander.cidenet.backend.model.Employee;
import com.alexander.cidenet.backend.service.EmployeeService;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/api/employees")
public class EmployeeController {

	// Inyección del servicio de empleados
	@Autowired
	private EmployeeService employeeService;

	// Crear nuevo empleado recibido por medio de la petición POST
	@PostMapping
	public ResponseEntity<?> create(@Valid @RequestBody Employee employee) {
		boolean exist = employeeService.findByTypeIdAndNumID(employee.getTypeId(), employee.getNumID()).isPresent();
		if(exist)  return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Identificación duplicada");
		Employee employeeSave = employeeService.save(employee);
		return ResponseEntity.status(HttpStatus.CREATED).body(employeeSave);
	
	}

	// Enviar empleados por medio de la petición GET
	@GetMapping
	public ResponseEntity<Map<String, Object>> list(@RequestParam Map<String, Object> params) {
		int page = params.get("page") != null ? Integer.parseInt(params.get("page").toString()) - 1 : 0;
		PageRequest pageRequest = PageRequest.of(page, 10);
		Page<Employee> pageEmployee = employeeService.findAll(pageRequest);
		Map<String, Object> response = new HashMap<>();
		response.put("employees", pageEmployee.getContent());
		response.put("totalEmployees", pageEmployee.getTotalElements());
		response.put("totalPages", pageEmployee.getTotalPages());
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	// Enviar empleado por medio de la petición GET
	@GetMapping("/{id}")
	public ResponseEntity<?> read(@PathVariable long id) {
		Optional<Employee> oEmployee = employeeService.findById(id);
		if (!oEmployee.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(oEmployee);
	}
	
	// Eliminar empleado con id recibido por medio de la petición DELETE
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable long id) {
		if (!validateEmployee(id)) {
			return ResponseEntity.notFound().build();
		}
		employeeService.deleteById(id);
		return ResponseEntity.ok("Empleado eliminado");
	}
	
	// Actualizar empleado con id recibido por medio de la petición PUT
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody Employee employee, @PathVariable long id) {
		try {
			Optional<Employee> oEmployee = employeeService.findById(id);
			if (!oEmployee.isPresent()) return ResponseEntity.notFound().build();
			
			if(employee.getTypeId() != oEmployee.get().getTypeId() || !employee.getNumID().equalsIgnoreCase(oEmployee.get().getNumID())) {
				boolean  exist = employeeService.findByTypeIdAndNumID(employee.getTypeId(), employee.getNumID()).isPresent();
				if (exist) return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Identificación duplicada");
			}
		
			Employee currentEmployee = oEmployee.get();
			currentEmployee.setArea(employee.getArea());
			currentEmployee.setOtherNames(employee.getOtherNames());
			currentEmployee.setSurnameTwo(employee.getSurnameTwo());
			currentEmployee.setNameOne(employee.getNameOne());
			currentEmployee.setSurnameOne(employee.getSurnameOne());
			currentEmployee.setCountry(employee.getCountry());
			currentEmployee.setTypeId(employee.getTypeId());
			currentEmployee.setNumID(employee.getNumID());
			
			Employee employeeUpdate = employeeService.save(currentEmployee);
			return ResponseEntity.status(HttpStatus.CREATED).body(employeeUpdate);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Faltan datos para actualizar");
		}
	}
	
	// Verificar si un empleado ya está guardado en la BD
	private boolean validateEmployee(long id) {
		Optional<Employee> oEmployee = employeeService.findById(id);
		return oEmployee.isPresent();	
	}
	
	
}
