package com.alexander.cidenet.backend.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.alexander.cidenet.backend.commons.Country;
import com.alexander.cidenet.backend.commons.TypeId;

@Entity
@Table(name = "employees")
@EntityListeners(AuditingEntityListener.class)
public class Employee  implements Serializable{

	private static final long serialVersionUID = -5000571358021532272L;
	
	/*
	 * Creación de modelo Empleado JPA para base de datos MySQL
	 * Se validan los valores recibidos para cada campo antes de tratarlos en la BD. (Middleware)
	 * Se añaden automaticamente los campos de creación y actualización de registros
	 * */

	// Atributos y columnas de la BD
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false, updatable = false)
	private long id;

	@NotEmpty(message = "El primer nombre es obligatorio")
	@Size(max = 20, message = "Debe tener máximo 20 caracteres")
	@Pattern(regexp = "[A-Z]+", message = "Solo puede tener letras mayusculas y sin acentos")
	@Column(name="name_one",length = 20, nullable = false)
	private String nameOne;
	
	@Size(max = 20, message = "Debe tener máximo 50 caracteres")
	@Pattern(regexp = "[A-Z]+", message = "Solo puede tener letras mayusculas y sin acentos")
	@Column(name="other_names",length = 50)
	private String otherNames;
	
	@NotEmpty(message = "El primer apellido es obligatorio")
	@Size(max = 20, message = "Debe tener máximo 20 caracteres")
	@Pattern(regexp = "[A-Z\\s]+", message = "Solo puede tener letras mayusculas y sin acentos")
	@Column(name="surname_one",length = 20, nullable = false)
	private String surnameOne;
	
	@NotEmpty(message = "El segundo apellido es obligatorio")
	@Size(max = 20, message = "Debe tener máximo 20 caracteres")
	@Pattern(regexp = "[A-Z\\s]+", message = "Solo puede tener letras mayusculas y sin acentos")
	@Column(name="surname_two",length = 20, nullable = false)
	private String surnameTwo;
	
	@NotEmpty(message = "El numero de identificación es obligatorio")
	@Size(max = 20, message = "Debe tener máximo 20 caracteres")
	@Pattern(regexp = "[a-zA-Z0-9-]+", message = "Solo puede tener letras mayusculas y sin acentos")
	@Column(name="num_id",length = 20, nullable = false)
	private String numID;
	
	@Email(message = "El email no es correcto")
	@Size(max = 300, message = "Debe tener máximo 300 caracteres")
	@Column(length = 300, unique = true, nullable = false)
	private String email;
	
	@Enumerated(EnumType.ORDINAL)
	private Country country;
	
	@ManyToOne
	@JoinColumn(name="area", referencedColumnName = "id")
	private Area area;
	
	@Column(name="type_id", nullable = false)
	@Enumerated(EnumType.ORDINAL)
	private TypeId typeId;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date admission;
	
	@Column(nullable = false, updatable = false)
	private boolean state = true;
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at", nullable = false, updatable = false)
    @CreatedDate
	private Date createdAt;
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at", nullable = false)
    @LastModifiedDate
	private Date updatedAt ;

	
	// Getters y Setters
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNameOne() {
		return nameOne;
	}

	public void setNameOne(String nameOne) {
		this.nameOne = nameOne;
	}

	public String getOtherNames() {
		return otherNames;
	}

	public void setOtherNames(String otherNames) {
		this.otherNames = otherNames;
	}

	public String getSurnameOne() {
		return surnameOne;
	}

	public void setSurnameOne(String surnameOne) {
		this.surnameOne = surnameOne;
	}

	public String getSurnameTwo() {
		return surnameTwo;
	}

	public void setSurnameTwo(String surnameTwo) {
		this.surnameTwo = surnameTwo;
	}

	public String getNumID() {
		return numID;
	}

	public void setNumID(String numID) {
		this.numID = numID;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public TypeId getTypeId() {
		return typeId;
	}

	public void setTypeId(TypeId typeId) {
		this.typeId = typeId;
	}

	public Date getAdmission() {
		return admission;
	}

	public void setAdmission(Date admission) {
		this.admission = admission;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
   
	
}
