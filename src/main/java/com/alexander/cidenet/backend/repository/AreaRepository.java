package com.alexander.cidenet.backend.repository;

import org.springframework.data.repository.CrudRepository;

import com.alexander.cidenet.backend.model.Area;

public interface AreaRepository extends CrudRepository<Area, Long> {

}
