package com.alexander.cidenet.backend.repository;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.alexander.cidenet.backend.commons.TypeId;
import com.alexander.cidenet.backend.model.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

	@Transactional(readOnly = true)
	Iterable<Employee> findByNameOneAndSurnameOne(String nameOne, String surnameOne);

	@Transactional(readOnly = true)
	Optional<Employee> findByTypeIdAndNumID(TypeId typeId, String numID);
	
}
