package com.alexander.cidenet.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class CidenetEmployeesApplication {

	public static void main(String[] args) {
		SpringApplication.run(CidenetEmployeesApplication.class, args);
	}

}
