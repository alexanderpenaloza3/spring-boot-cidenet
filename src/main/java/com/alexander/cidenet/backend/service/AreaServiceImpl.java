package com.alexander.cidenet.backend.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alexander.cidenet.backend.model.Area;
import com.alexander.cidenet.backend.repository.AreaRepository;

@Service
@Transactional
public class AreaServiceImpl implements AreaService {
	
	// Inyección del repositorio de areas
	@Autowired
	private AreaRepository areaRepository;

	@Override
	public Iterable<Area> findAll() {
		return areaRepository.findAll();
	}

	@Override
	public Area save(Area area) {
		return areaRepository.save(area);
	}

	@Override
	public Optional<Area> findById(long id) {
		return areaRepository.findById(id);
	}

}
