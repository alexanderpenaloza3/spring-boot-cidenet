package com.alexander.cidenet.backend.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.alexander.cidenet.backend.commons.TypeId;
import com.alexander.cidenet.backend.model.Employee;

public interface EmployeeService {

	// Listar todos los empleados
	public Iterable<Employee> findAll();
	// Listar empleados paginados
	public Page<Employee> findAll(Pageable pageable);
	// Obtener empleado
	public Optional<Employee> findById(long id);
	// Guardar empleado
	public Employee save(Employee employee);
	//  Eliminar empleado
	public void deleteById(long id);
	// Obtener empleado por tipo e identificación para validar repetidos
	public Optional<Employee> findByTypeIdAndNumID(TypeId typeId, String numID);
}
