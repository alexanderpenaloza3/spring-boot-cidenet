package com.alexander.cidenet.backend.service;

import java.util.Optional;

import com.alexander.cidenet.backend.model.Area;

public interface AreaService {
	
	// Listar Areas
	public Iterable<Area> findAll();
	// Guardar Area
	public Area save(Area area);
	// Obtener Area
	public Optional<Area> findById(long id);
	
}
