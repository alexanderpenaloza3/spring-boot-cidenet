package com.alexander.cidenet.backend.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alexander.cidenet.backend.commons.TypeId;
import com.alexander.cidenet.backend.model.Area;
import com.alexander.cidenet.backend.model.Employee;
import com.alexander.cidenet.backend.repository.AreaRepository;
import com.alexander.cidenet.backend.repository.EmployeeRepository;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

	// Inyección de repositorios de areas y empleados
	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Autowired AreaRepository areaRepository;
	
	// Métodos
	
	@Override
	public Iterable<Employee> findAll() {
			return employeeRepository.findAll();
	}

	@Override
	public Page<Employee> findAll(Pageable pageable) {
		return employeeRepository.findAll(pageable);
	}

	@Override
	public Optional<Employee> findById(long id) {
		return employeeRepository.findById(id);
	}

	@Override
	public Employee save(Employee employee) {
		// Obtener Area del empleado
		long idArea = employee.getArea().getId();
		Optional<Area> area = areaRepository.findById(idArea);
		employee.setArea(area.get());
		// Construir correo
		// 1. Verificar si existe correo con los mismos nombres y apellidos
		List<Employee> employees = StreamSupport.stream(employeeRepository.findByNameOneAndSurnameOne(employee.getNameOne(), employee.getSurnameOne()).spliterator(), false).collect(Collectors.toList());
		int count = employees.size();
		// 2. Añadir id si es necesario
		String id = count > 0 ? "."+count : "";
		// 3. Crear formato del correo
		String email = employee.getNameOne().trim() + "."+ employee.getSurnameOne().trim()+id+"@cidenet.com."+employee.getCountry();
		employee.setEmail(email.toLowerCase().replace(" ", ""));
		// Guardar empleado
		return employeeRepository.save(employee);
	}

	@Override
	public void deleteById(long id) {
		employeeRepository.deleteById(id);
	}

	@Override
	public Optional<Employee> findByTypeIdAndNumID(TypeId typeId, String numID) {
		return employeeRepository.findByTypeIdAndNumID(typeId, numID);
	}

}
