package com.alexander.cidenet.backend.commons;


// Enumerador para el País de empleo
public enum Country {
	CO("Colombia"),
	US("Estados Unidos");
	
	// Atributos
	private String value;

	// Constructor
	private Country(String value) {
		this.value = value;
	}

	// Getter
	public String getValue() {
		return value;
	}
	
	
}
