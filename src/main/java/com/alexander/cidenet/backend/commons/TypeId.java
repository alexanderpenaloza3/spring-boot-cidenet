package com.alexander.cidenet.backend.commons;

//Enumerador para el tipo de identificación
public enum TypeId {

	CC("Cédula de ciudadanía"),
	CE("Cédula de extrangería"),
	PP("Pasaporte"),
	PE("Permiso especial");
	
	// Atributos
	private String value;

	// Constructor
	private TypeId(String value) {
		this.value = value;
	}

	// Getter
	public String getValue() {
		return value;
	}
	
	
}
